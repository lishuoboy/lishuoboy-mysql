drop table if exists demo;
create table demo
(
    `id`  bigint not null auto_increment comment '8个字节。单列索引、聚簇索引（一级索引）、主键索引。',
    `id2` bigint default null comment '8个字节。单列索引、非聚簇索引（二级索引）、唯一索引。',
    `id3` bigint default null comment '8个字节。单列索引、非聚簇索引（二级索引）、普通索引。',
    `id4` bigint default null comment '8个字节。无索引。',
    primary key (`id`),
    unique key `id2` (`id2`),
    key `id3` (`id3`)
) default charset = gbk comment ='演示';
