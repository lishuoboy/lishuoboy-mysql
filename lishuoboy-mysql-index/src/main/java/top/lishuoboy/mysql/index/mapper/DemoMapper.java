package top.lishuoboy.mysql.index.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.lishuoboy.mysql.index.domain.Demo;

/**
 * @author lishuoboy
 * @description 针对表【demo(演示)】的数据库操作Mapper
 * @createDate 2023-08-06 22:40:28
 * @Entity top.lishuoboy.mysql.index.domain.Demo
 */
public interface DemoMapper extends BaseMapper<Demo> {

}




