package top.lishuoboy.mysql.index.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 演示
 *
 * @TableName demo
 */
@TableName(value = "demo")
@Data
@Builder
public class Demo implements Serializable {
    /**
     * 8个字节。单列索引、聚簇索引（一级索引）、主键索引。
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 8个字节。单列索引、非聚簇索引（二级索引）、唯一索引。
     */
    private Long id2;

    /**
     * 8个字节。单列索引、非聚簇索引（二级索引）、普通索引。
     */
    private Long id3;

    /**
     * 8个字节。无索引。
     */
    private Long id4;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
