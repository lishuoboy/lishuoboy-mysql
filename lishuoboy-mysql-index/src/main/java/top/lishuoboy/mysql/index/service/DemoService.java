package top.lishuoboy.mysql.index.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.lishuoboy.mysql.index.domain.Demo;

/**
 * @author lishuoboy
 * @description 针对表【demo(演示)】的数据库操作Service
 * @createDate 2023-08-06 22:40:28
 */
public interface DemoService extends IService<Demo> {

}
