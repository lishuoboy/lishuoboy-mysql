package top.lishuoboy.mysql.index;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import top.lishuoboy.dependency.sb.sb.MySbUtil;

@MapperScan("top.lishuoboy.mysql.index.mapper")
@SpringBootApplication
public class MysqlIndexApp {

    public static void main(String[] args) {
        MySbUtil.run(MysqlIndexApp.class, args);
    }

}
