package top.lishuoboy.mysql.index.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.lishuoboy.mysql.index.domain.Demo;
import top.lishuoboy.mysql.index.mapper.DemoMapper;
import top.lishuoboy.mysql.index.service.DemoService;

/**
 * @author lishuoboy
 * @description 针对表【demo(演示)】的数据库操作Service实现
 * @createDate 2023-08-06 22:40:28
 */
@Service
public class DemoServiceImpl extends ServiceImpl<DemoMapper, Demo> implements DemoService {

}




